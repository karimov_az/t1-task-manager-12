package ru.t1.karimov.tm.api;

import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create (String name, String description);

    Project create (String name);

    Project add(Project project);

    void clear();

    List<Project> findAll();

    void remove(Project project);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project changeProjectStatusById (String id, Status status);

    Project changeProjectStatusByIndex (Integer index, Status status);

    int getSize();

}
